/**
 * cookies
 *
 * cookie.set({
        name  : 'aaa',
        value : 'bbb',
        domain: 'm.jd.com',
        path  : '',
        time  : '+{1y1m11d1h1n1s}'      + 在现在的基础上加上多久失效 y年 m月 d日 h时  n分  s秒
    });
 *
 * cookie.set({
        name  : 'aaa',
        value : 'bbb',
        domain: 'm.jd.com',
        path  : '',
        time  : '>{2018-1-10 10:6:30}'    > 到日期失效
    });
 *
 *
 *
 */
var cookie     = {
    get: function (name) {
        var reg = new RegExp("(?:;\\s+)(" + name + ")=(.*?)(?:;)");
        var o   = ('; ' + document.cookie).match(reg);
        return !o ? null : decodeURI(o[2]);
    },
    set: function (o) {
        var oTime = new Date();
        if (o.time) {
            if (typeof o.time === 'string') {
                var timeTmp = o.time.substr(0, 1).replace('', '');
                if (timeTmp === '+') {
                    var aTime = o.time.substring(2, o.time.length - 1).split('-');
                    for (var i = 0; i < aTime.length; i++) {
                        var sTime = aTime[i].match(/([0-9]*)([y|m|d|h|n|s])/);
                        if (sTime[2] === 'y') {
                            oTime.setFullYear(new Date().getFullYear() + parseInt(sTime[1]));
                        }
                        if (sTime[2] === 'm') {
                            oTime.setMonth(new Date().getMonth() + parseInt(sTime[1]));
                        }
                        if (sTime[2] === 'd') {
                            oTime.setDate(new Date().getDate() + parseInt(sTime[1]));
                        }
                        if (sTime[2] === 'h') {
                            oTime.setHours(new Date().getHours() + parseInt(sTime[1]));
                        }
                        if (sTime[2] === 'n') {
                            oTime.setMinutes(new Date().getMinutes() + parseInt(sTime[1]));
                        }
                        if (sTime[2] === 's') {
                            oTime.setSeconds(new Date().getSeconds() + parseInt(sTime[1]));
                        }

                    }
                }
                if (timeTmp === '>') {
                    var toTime = o.time.match(/(>{)([0-9]{4}-[0-9]{1,2}-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2})/)[2];
                    oTime      = new Date(toTime);
                }
            }
        }
        document.cookie = o.name + '=' + encodeURI(o.value) + ';domain=' + o.domain + ';expires=' + oTime + ';path=' + o.path || '/';
    },
    del: function (name, path, domain) {
        if (arguments.length == 2) {
            domain = path;
            path   = "/"
        }
        document.cookie = name + "=;path=" + path + ";" + (domain ? ("domain=" + domain + ";") : '') + "expires=Thu, 01-Jan-70 00:00:01 GMT";
    }
};
module.exports = cookie;