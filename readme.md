#MC-PRECODE

一款JS代码语法检测高亮显示插件，这款插件可以对全部的pre标签或指定class或id的pre标签内的JS代码进行语法检测并语法高亮显示！**本插件已停止维护，更名为mc-tinting https://gitlab.com/fekits/mc-tinting**



### 目录索引
* [功能演示](#功能演示)
* [使用文档](#使用文档)
* [开发文档](#开发文档)
* [版本说明](#版本说明)

<br>

### 功能演示
[http://www.junbo.name/plugins/precode/](http://www.junbo.name/plugins/precode/)

<br>

### 使用文档

传参说明：
```javascript
    new PreCode({
        el    : '.precode',   // {String}  指定需要风亮显示的<pre>标签   [选填] 默认为全部<pre>标签
        theme : 'vcode',      // {String}  指定一个主题风格             [选填] 默认为 "default"
    })
```

引用插件：

插件为了可以广泛应用于各种场景，预写了多种调用方式；

// 模块开发
```javascript
// 引入模块
import PreCode from './js/mc-precode.min';  // 模块化请使用cmd版本
// 创建一个实例
new PreCode();

```

// 直接引用
```html
    
    <!--直接在页面引用插件文件-->
    <script src="http://www.junbo.name/plugins/precode/js/mc-precode.min.js"></script>
    <script>
        new window.fekit.PreCode();
    </script>

```

使用示例：

1、使用起来实在太简单了，使用示例如下：
```javascript
    import './js/mc-precode.min';
    new PreCode()  // 全局使用默认设置
```

2、只想让指定的标签高亮显示,示例如下：
```html
<script src="http://www.junbo.name/plugins/precode/js/mc-precode.min.js"></script>
<script>
    new window.fekit.PreCode({
       el:'.aaa'  // 只对class为aaa的pre标答开启代码检测和高亮显示 
    });
</script>
```

3、你喜欢WebStorm还是vcode的代码主题风格？这样可以指定一款你喜欢的主题风格
```javascript
    import './js/mc-precode.min';
    new PreCode({
       theme:'vcode'  // 只对class为aaa的<pre>标答开启代码检测和高亮显示 
    });
```

<br>

### 开发文档

下载项目: 
```
git clone https://gitlab.com/fekits/mc-precode.git
```
安装依赖: 
```
npm i

```
运行项目: 
```
gulp dev                                      // 开发环境（运行后监听文件修改自动打包）
gulp pro                                      // 生产环境
```

开发目录：
```
jmdd-floor-track');
├── dist/
│   ├── css                                   // 打包压缩后的文件       
│   │   ├── main.min.css                   
│   │   ├── mc-pre-code-default.min.css                   
│   │   ├── mc-pre-code-vcode.min.css                   
│   │   └── ...
│   │   
│   ├── js                                
│   │   ├── main.min.js                   
│   │   ├── mc-pre-code.min.js                // 这个就是jmdd-floor-track插件生产版本文件
│   │   └── ...
│   │   
│   ├── index.html                            // 示例文件（看示例效果请看这个文件）
│   └── ...
│
├── kits/                                     // 开发工具库
│   ├── css      
│   │   ├── ...                   
│   │   └── ...
│   │   
│   ├── js                                
│   │   ├── api                               // 常用方法
│   │   ├── plugins                           // 常用插件
│   │   └── js                                
│   │   
│   └── ...
│   
├── src/
│   ├── css    
│   │   ├── main.scss                   
│   │   ├── mc-pre-code@theme=default.scss                   
│   │   ├── mc-pre-code@theme=vcode.scss                   
│   │   └── ...
│   │                              
│   ├── js                                
│   │   ├── main.js                           // 示例用法看这个文件
│   │   ├── mc-precode.js                     // 这个就是自适应插件开发版源文件  *
│   │   └── ...
│   │   
│   ├── build.js                              // 打包配置文件
│   ├── index.html                            // 示例文件源文件
│   └── ...
│
├── tasks/                                    // 打包模块化子任务
│   ├── conf                                  
│   ├── task-build.js                         
│   ├── task-css.js                           
│   ├── task-html.js                          
│   ├── task-img.js                           
│   ├── task-js.js                            
│   └── ...
│  
├── gulpfile.js                               // 打包任务
│
└── ...
```

<br>

### 版本说明
当前版本：v0.0.1
