/**
 * 默认配置
 *
 * 这里是全局默认配置，在单个配置中默认是使用这个全局默认配置的，如果要自定义则把这个斌值换成自己写的一个配置对象即可！
 * 设置参考文档 docs/build.md
 *
 *
 * **/
const config = require('../tasks/conf');

/**
 * 打包配置
 *
 * 1、把需要打包到一起的JS写在同一个src的数组中。
 * 2、如果单个配置中有自定义的配置参数，则整个conf下的所有设置都需要手动配置。--这个以后会改善！！！
 *
 * **/
module.exports = {
    info: {
        name   : 'demo',
        version: '0',
        title  : '构建框架DEMO',
        branch : 'base',
        author : 'xiaojunbo'
    },
    conf: config,
    file: {
        html : [
            {
                name: [
                    'index.html'
                ],
                ins : [
                    {
                        pos: 'head',
                        src: [
                            'css/main.min.css'
                        ]
                    }, {
                        pos: 'body',
                        src: [
                            'js/main.min.js'
                        ]
                    }
                ],
                src : [
                    '{item}/index.html'
                ],
                min : false
            }
        ],
        css  : [
            {
                name: 'main.css',
                src : [
                    '{item}/css/main.scss'
                ]
            },
            {
                name: 'mc-pre-code-default.css',
                src : [
                    '{item}/css/mc-pre-code@theme=default.scss'
                ]
            },
            {
                name: 'mc-pre-code-vcode.css',
                src : [
                    '{item}/css/mc-pre-code@theme=vcode.scss'
                ]
            }
        ],
        js   : [
            {
                name  : 'main.js',
                src   : [
                    '{item}/js/main.js'
                ],
                syntax: 'es6',
                cmd   : true
            },
            {
                name  : 'mc-precode.js',
                src   : [
                    '{item}/js/mc-precode.js'
                ],
                syntax: 'es6',
                cmd   : true
            }
        ],
        img  : [
            {
                src: [
                    '{item}/img/*.{png,jpg,gif,ico}'
                ],
                min: false
            }
        ],
        media: [
            {
                src: [
                    '{item}/media/*.{mp3,mp4}'
                ]
            }
        ]
    },
    then: {}
};
