import PreCode from './mc-precode';

// 第一个实例
new PreCode({
    el   : '.demo',
    theme: 'darcula'
});

// 第二个实例
new PreCode({
    el   : '.precode',
    theme: 'darcula'
});
