/**
 * <pre>标签javascript语法高亮显示插件，目的是对写在<pre>标签的内的源代码示例自动加上语法高亮。
 *
 * @param   conf.el                指定需要语法高亮的DOM节点
 * @param   conf.functionName      函数名称高亮
 * @param   conf.newFunctionName   构造函数名称高亮
 * @param   conf.allFunctionName   全局构造函数名称高亮
 * @param   conf.keyword           关键词高亮 [import,from,class,let,var,this]
 * @param   conf.string            字符串高亮
 * @param   conf.key               属性名高亮
 * @param   conf.val               属性值高亮
 * @param   conf.val               属性值高亮
 * @param   conf.symbol            符号高亮 [;,]
 * */

class McPrecode {
    constructor(conf) {
        // 实例指定的主题
        this.theme = conf.theme || 'default';
        // this.lang  = 'javascript';
        
        // 实例全部的DOM节点仓库
        this.aPreDom = [];
        
        let preCssCode = (resCode) => {
            return resCode;
        };
        
        let preJsCode = (resCode) => {
            let newCode = resCode;
            
            // 正则内部处理
            function regCode(s3) {
                let s3Data = [];
                // 转义符处理
                s3         = s3.replace(/(\\+[wsndbrtWSNDB.\\^$/]|\(|\)|\[|])/g, (s1) => {
                    s3Data.push(`<code class="j-reg-spe">${ s1 }</code>`);
                    return `{__REG_S3DATA${ s3Data.length }__}`;
                });
                
                // 特殊字符处理
                s3 = s3.replace(/(\^|\||\\\*)/g, (s1) => {
                    s3Data.push(`<code class="j-sym">${ s1 }</code>`);
                    return `{__REG_S3DATA${ s3Data.length }__}`;
                });
                
                // 运算符处理
                s3 = s3.replace(/(\+|\*|\?)/g, (s1) => {
                    s3Data.push(`<code class="j-arithmetic">${ s1 }</code>`);
                    return `{__REG_S3DATA${ s3Data.length }__}`;
                });
                
                // 回收
                s3Data.forEach((item, index) => {
                    let reg = new RegExp(`{__REG_S3DATA${ index + 1 }__}`, 'g');
                    s3      = s3.replace(reg, item);
                });
                return s3;
            }
            
            // 注释处理
            (() => {
                
                // 注释
                let comData = [];
                
                // 单行注释
                newCode = newCode.replace(/([^"']\/\/[^:][^\n]*)/g, ($1) => {
                    comData.push(`<code class="j-com-line">${ $1 }<code class="j-com-line-end"></code></code>`);
                    return `{__COM_PRECODE${ comData.length }__}`;
                });
                // 多行注释(单星号)
                newCode = newCode.replace(/(\/\*[^*][\s\S]*?\*\/)/g, (g1) => {
                    comData.push(`<code class="j-com-more">${ g1 }</code>`);
                    return `{__COM_PRECODE${ comData.length }__}`;
                });
                // 多行注释(双星号)
                newCode = newCode.replace(/(\/\*\*[\s\S]*?\*\/)/g, ($1) => {
                    $1 = $1.replace(/(@param)(\s+)/g, '<code class="j-com-param">$1</code>$2');
                    comData.push(`<code class="j-com-mode">${ $1 }</code>`);
                    return `{__COM_PRECODE${ comData.length }__}`;
                });
                
                // 逻辑符号
                // let symCode = [];
                // newCode     = newCode.replace(/(&&|===|==|\|\||\+=|-=|\*=|\/=|<=|>=|&amp;&amp;)/g, (s1) => {
                //     symCode.push(`<code class="j-sym">${ s1 }</code>`);
                //     return `{__SYM_PRECODE${ symCode.length }__}`;
                // });
                
                // // 箭头函数
                // newCode = newCode.replace(/(\s+=>\s+|\s+)(=&gt;)(\s+)/g, (s0, s1, s2, s3) => {
                //     codeData.push(`<code>${ s2 }</code>`);
                //     return `${ s1 }{__PRECODE${ codeData.length }__}${ s3 }`;
                // });
                //
                // // HTML标签
                // newCode = newCode.replace(/(&lt;\/?)(link|a|abbr|acronym|address|applet|area|article|aside|audio|b|base|basefont|bdi|bdo|big|blockquote|body|br|button|canvas|caption|center|cite|code|col|colgroup|command|datalist|dd|del|details|dfn|dialog|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frame|frameset|h1|h2|h3|h4|h5|h6|head|header|hr|html|i|iframe|img|input|ins|kbd|keygen|label|legend|li|link|main|map|mark|menu|menuitem|meta|meter|nav|noframes|noscript|object|ol|optgroup|option|output|p|param|progress|q|rp|rt|ruby|s|samp|script|section|select|small|source|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video|wbr)(\s*.*?)(&gt;)/gm, (s0, s1, s2, s3, s4) => {
                //
                //     // console.log('120', s3);
                //
                //     s3 = s3.replace(/(\w+)(=)('|")(.*?)(\3)/g, (s0, s1, s2, s3, s4, s5) => {
                //         return `<code class="co-oth">${ s1 }</code><code class="co-equ">${ s2 }</code><code class="co-str">${ s3 }${ s4 }${ s5 }</code>`;
                //     });
                //
                //     codeData.push(`<code class="co-tag"><code>${ s1 }</code><code>${ s2 }</code><code>${ s3 }</code><code>${ s4 }</code></code>`);
                //     return `{__PRECODE${ codeData.length }__}`;
                // });
                
                // 尖括号
                let pobData = [];
                newCode     = newCode.replace(/(<|>|&lt;|&gt;)/g, (s1) => {
                    if(s1 === '<' || s1 === '&lt;') {
                        pobData.push(`<code class="j-lt">&lt;</code>`);
                    }
                    
                    if(s1 === '>' || s1 === '&gt;') {
                        pobData.push(`<code class="j-gt">&gt;</code>`);
                    }
                    return `{__POB_PRECODE${ pobData.length }__}`;
                });
                
                // 正则
                let regData = [];
                newCode     = newCode.replace(/(\.replace\()(\/)([^\n]*)(\/)(g?|i?|m?|s?|u?|y?)(,)/g, ($0, $1, $2, $3, $4, $5, s6) => {
                    // 正则内部处理
                    $3 = regCode($3);
                    regData.push(`<code class="j-reg">${ $2 }</code><code class="j-reg-str">${ $3 }</code><code class="j-reg">${ $4 }</code><code class="j-str">${ $5 }</code><code class="j-sym">${ s6 }</code>`);
                    return `${ $1 }{__REG_PRECODE${ regData.length }__}`;
                });
                
                // 正则-字符串
                newCode = newCode.replace(/(\/)([^\n]*)(\/)(\.test\()/g, ($0, $1, $2, $3, $4) => {
                    // 正则内部处理
                    $2 = regCode($2);
                    regData.push(`<code class="j-reg">${ $1 }<code class="j-reg-str">${ $2 }</code>${ $3 }</code>`);
                    return `{__REG_PRECODE${ regData.length }__}${ $4 }`;
                });
                
                // 正则-构造函数
                newCode = newCode.replace(/(new\sRegExp\()('|")([^\n]*)(\2)\s?(,)\s?('|")(g?|i?|m?|s?|u?|y?)(\6)/g, ($0, $1, $2, s3, $4, $5, $6, $7, $8) => {
                    // 正则内部处理
                    s3 = regCode(s3);
                    regData.push(`<code class="j-str">${ $2 }</code><code class="j-reg"><code class="j-reg-str">${ s3 }</code><code class="j-str">${ $4 }</code><code class="j-sym">${ $5 }</code><code class="j-str">${ $6 }${ $7 }${ $8 }</code></code>`);
                    return `${ $1 }{__REG_PRECODE${ regData.length }__}`;
                });
                
                // 字符串
                let strData = [];
                newCode     = newCode.replace(/('|")[^\n(\1)]*?\1/g, ($1) => {
                    strData.push(`<code class="j-str">${ $1 }</code>`);
                    return `{__STR_PRECODE${ strData.length }__}`;
                });
                
                // 函数变量
                // let varData = [];
                // newCode = newCode.replace(/\b(let\s+|var\s+)(\w+)(\s*=\s*)/gm, ($0, $1, $2, $3) => {
                //     varData.push(`<code class="co-var">${ $2 }</code>`);
                //     return `${ $1 }{__FUN_PRECODE${ varData.length }__}${ $3 }`;
                // });
                
                // 事件名称
                let eveData = [];
                newCode     = newCode.replace(/(\.)(\w+)(\s*=\s*)(\(|function\s*\()/g, (s0, s1, s2, s3, s4) => {
                    eveData.push(`<code class="j-eve">${ s2 }</code>`);
                    return `${ s1 }{__EVE_PRECODE${ eveData.length }__}${ s3 }${ s4 }`;
                });
                
                // 函数名称
                let funData = [];
                newCode     = newCode.replace(/(\.|\b)(\w*)(\()/g, (s0, $1, $2, $3) => {
                    funData.push(`<code class="j-fun">${ $2 }</code>`);
                    return `${ $1 }{__FUN_PRECODE${ funData.length }__}${ $3 }`;
                });
                
                // 关键词
                let sysData = [];
                newCode     = newCode.replace(/(\s*|\b)(let|var|import|from|class|new|this|export\sdefault)(\s+|\b)/g, (s0, s1, s2, s3) => {
                    sysData.push(`<code class="j-ret">${ s2 }</code>`);
                    return `${ s1 }{__SYS_PRECODE${ sysData.length }__}${ s3 }`;
                });
                // 逻辑符
                newCode     = newCode.replace(/(\s*|\b)(false|true|for|if|else|return)(\s+|\b)/g, (s0, s1, s2, s3) => {
                    sysData.push(`<code class="j-logic">${ s2 }</code>`);
                    return `${ s1 }{__SYS_PRECODE${ sysData.length }__}${ s3 }`;
                });
                
                // 对象属性
                let objData = [];
                newCode     = newCode.replace(/([^=][,{\s\n]+)([^,\n\s.({})':=]*)(\s*:)/g, '$1<code class="j-key">$2</code>$3');
                newCode     = newCode.replace(/([^=][,{\s\n]+)([^,\n\s.({})':=]*)(\s*:)/g, (s0, s1, s2, s3) => {
                    objData.push(`<code class="j-key">${ s2 }</code>`);
                    return `${ s1 }{__OBJ_PRECODE${ objData.length }__}${ s3 }`;
                });
                
                // // 对象子集
                let subData = [];
                newCode     = newCode.replace(/(\.)([a-zA-Z]+[0-9]*)\b/g, (s0, s1, s2) => {
                    subData.push(`<code class="j-sub">${ s2 }</code>`);
                    return `${ s1 }{__SUB_PRECODE${ subData.length }__}`;
                });
                
                // 数字
                let numData = [];
                newCode     = newCode.replace(/([\s+\+=\-\*\/:,])(-?\d+\.?\d*)\b/g, (s0, s1, s2) => {
                    numData.push(`<code class="j-num">${ s2 }</code>`);
                    return `${ s1 }{__NUM_PRECODE${ numData.length }__}`;
                });
                
                // // 符号
                // let symData = [];
                // newCode = newCode.replace(/(,|;)/gm, (s1) => {
                //     symData.push(`<code class="j-sym">${ s1 }</code>`);
                //     return `{__SYM_PRECODE${ symData.length }__}`;
                // });
                //
                // // 运算符处理
                // let opeData = [];
                // newCode = newCode.replace(/(\+|\*)/g, (s1) => {
                //     opeData.push(`<code class="j-ope">${ s1 }</code>`);
                //     return `{__OPE_PRECODE${ opeData.length }__}`;
                // });
                //
                // // 点，冒号，括号
                // let dotData = [];
                // newCode = newCode.replace(/(\.|:|\(|\)|\[|\]|\?)/g, (s1) => {
                //     dotData.push(`<code class="j-dot">${ s1 }</code>`);
                //     return `{__DOT_PRECODE${ dotData.length }__}`;
                // });
                //
                // // 等于号
                // let equData=[];
                // newCode = newCode.replace(/(=)/g, (s1) => {
                //     equData.push(`<code class="co-equ">${ s1 }</code>`);
                //     return `{__EQU_PRECODE${ equData.length }__}`;
                // });
                //
                // // 其它单词
                // let othData = [];
                // newCode = newCode.replace(/(}\s*)([\w$]+)(\s*{)/g, (s0, s1, s2, s3) => {
                //     othData.push(`<code class="co-oth">${ s2 }</code>`);
                //     return `${ s1 }{__OTH_PRECODE${ othData.length }__}${ s3 }`;
                // });
                //
                //
                //
                // // 其它还原
                // othData.forEach((item, index) => {
                //     let reg = new RegExp(`{__OTH_PRECODE${ index + 1 }__}`, 'g');
                //     newCode = newCode.replace(reg, item);
                // });
                //
                // // 其它还原
                // equData.forEach((item, index) => {
                //     let reg = new RegExp(`{__EQU_PRECODE${ index + 1 }__}`, 'g');
                //     newCode = newCode.replace(reg, item);
                // });
                //
                // // 运算符还原
                // dotData.forEach((item, index) => {
                //     let reg = new RegExp(`{__DOT_PRECODE${ index + 1 }__}`, 'g');
                //     newCode = newCode.replace(reg, item);
                // });
                //
                // // 运算符还原
                // opeData.forEach((item, index) => {
                //     let reg = new RegExp(`{__OPE_PRECODE${ index + 1 }__}`, 'g');
                //     newCode = newCode.replace(reg, item);
                // });
                //
                // // 符号还原
                // symData.forEach((item, index) => {
                //     let reg = new RegExp(`{__SYM_PRECODE${ index + 1 }__}`, 'g');
                //     newCode = newCode.replace(reg, item);
                // });
                
                // 数字还原
                numData.forEach((item, index) => {
                    let reg = new RegExp(`{__NUM_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 子集还原
                subData.forEach((item, index) => {
                    let reg = new RegExp(`{__SUB_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 对象还原
                objData.forEach((item, index) => {
                    let reg = new RegExp(`{__OBJ_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 系统还原
                sysData.forEach((item, index) => {
                    let reg = new RegExp(`{__SYS_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 对象还原
                funData.forEach((item, index) => {
                    let reg = new RegExp(`{__FUN_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 对象还原
                eveData.forEach((item, index) => {
                    let reg = new RegExp(`{__EVE_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 注释还原
                strData.forEach((item, index) => {
                    let reg = new RegExp(`{__STR_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 注释还原
                regData.forEach((item, index) => {
                    let reg = new RegExp(`{__REG_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 注释还原
                comData.forEach((item, index) => {
                    let reg = new RegExp(`{__COM_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // 注释还原
                pobData.forEach((item, index) => {
                    let reg = new RegExp(`{__POB_PRECODE${ index + 1 }__}`, 'g');
                    newCode = newCode.replace(reg, item);
                });
                
                // let lineNum = 0;
                // newCode     = newCode.replace(/.*\n{1}/g, (g1) => {
                //     lineNum += 1;
                //     return `<code class="j-line"><code class="j-line-num">${ lineNum }</code><code class="j-line-code">${ g1 }</code></code>`;
                // });
                
            })();
            return newCode;
        };
        
        window.addEventListener('load', () => {
            // 入参是否指定DOM?
            let el = conf && conf.el;
            if(el) {
                // 传入的是CLASS类型
                if(/^\./.test(el)) {
                    // 去掉CLASS前面的.
                    let className = el.replace(/\./, '');
                    
                    // 选择DOM节点(为了兼容IE低版本，这里没有使用getElementsByClassName,而是写了一个兼容IE8的版本)
                    // 先选择全部的<pre>标签
                    let tags = document.getElementsByTagName('pre'), tagsLength = tags.length, cName = ' ' + className + ' ', aTagDom = [];
                    // 选从中选出有指定CLASS的标签
                    for(let i = 0; i < tagsLength; i++) {
                        if(tags[i].className && (' ' + tags[i].className + ' ').indexOf(cName) !== -1) {
                            aTagDom.push(tags[i]);
                        }
                    }
                    this.aPreDom = aTagDom;
                }
                // 传入的是ID类型
                else if(/^#/.test(el)) {
                    // 去掉CLASS前面的#
                    let id = el.replace(/#/, '');
                    
                    // 选择DOM节点
                    let aIdDom   = document.getElementById(id);
                    this.aPreDom = aIdDom ? [aIdDom] : [];
                }
            } else {
                // 如果没有指定DOM的选择器则默认获取全局的<pre>标签,并且筛选一下lang是不是javascript.
                this.aPreDom = document.getElementsByTagName('pre');
            }
            
            console.log(this.aPreDom);
            
            let preNum = this.aPreDom.length;
            for(let i = 0; i < preNum; i++) {
                // 处理语法高亮
                (() => {
                    // 获取单个标签
                    let theDom = this.aPreDom[i];
                    console.log(theDom);
                    
                    // 加入主题标识
                    theDom.setAttribute('theme', this.theme);
                    
                    // 查询语言标识
                    let theLang = theDom.getAttribute('lang');
                    console.log(theLang);
                    // 创建一个副本
                    let newCode = theDom.innerHTML;
                    
                    // HTML语法处理
                    if(theLang === 'html') {
                        
                        // 括号处理
                        newCode = newCode.replace(/</g, '&lt;').replace(/>/g, '&gt;');
                        
                        // 内联样式
                        let cssCode = [];
                        newCode     = newCode.replace(/(&lt;style\s*&gt;)([\s\S]*?)(&lt;\/style&gt;)/g, (g0, g1, g2, g3) => {
                            console.log('g1', g1);
                            console.log('g2', g2);
                            console.log('g3', g3);
                            cssCode.push(preCssCode(g2));
                            return `${ g1 }{__CSS_PRECODE${ cssCode.length }__}${ g3 }`;
                        });
                        
                        // 内联JS
                        let jsCode = [];
                        newCode    = newCode.replace(/(&lt;script\s*&gt;)([\s\S]*?)(&lt;\/script&gt;)/g, (g0, g1, g2, g3) => {
                            console.log('g1', g1);
                            console.log('g2', g2);
                            console.log('g3', g3);
                            jsCode.push(preJsCode(g2));
                            console.log(jsCode);
                            return `${ g1 }{__JS_PRECODE${ jsCode.length }__}${ g3 }`;
                        });
                        
                        // 注释处理
                        let comCode = [];
                        newCode     = newCode.replace(/&lt;!--(.|\n)*?--&gt;/g, (g1) => {
                            comCode.push(`<code class="h-com">${ g1 }</code>`);
                            return `{__COM_PRECODE${ comCode.length }__}`;
                        });
                        
                        // 属性处理
                        let keyCode = [];
                        newCode     = newCode.replace(/(\s\w*)(=)(["'][^"']*["'])/g, (g0, g1, g2, g3) => {
                            keyCode.push(`<code class="h-key">${ g1 }${ g2 }</code><code class="h-val">${ g3 }</code>`);
                            return `{__KEY_PRECODE${ keyCode.length }__}`;
                        });
                        
                        // 标签处理
                        let tagCode = [];
                        newCode     = newCode.replace(/(&lt;)(\/?[a-zA-Z]+)([\s\S]*?)(&gt;)/g, (g0, g1, g2, g3, g4) => {
                            tagCode.push(`<code class="h-tag">${ g1 }${ g2 }</code>${ g3 }<code class="h-tag">${ g4 }</code>`);
                            return `{__TAG_PRECODE${ tagCode.length }__}`;
                        });
                        
                        
                        // 标签还原
                        tagCode.forEach((item, index) => {
                            let reg = new RegExp(`{__TAG_PRECODE${ index + 1 }__}`, 'g');
                            newCode = newCode.replace(reg, item);
                        });
                        
                        
                        // 属性还原
                        keyCode.forEach((item, index) => {
                            let reg = new RegExp(`{__KEY_PRECODE${ index + 1 }__}`, 'g');
                            newCode = newCode.replace(reg, item);
                        });
                        
                        // 注释还原
                        comCode.forEach((item, index) => {
                            let reg = new RegExp(`{__COM_PRECODE${ index + 1 }__}`, 'g');
                            newCode = newCode.replace(reg, item);
                        });
                        
                        // JS还原
                        jsCode.forEach((item, index) => {
                            let reg = new RegExp(`{__JS_PRECODE${ index + 1 }__}`, 'g');
                            newCode = newCode.replace(reg, item);
                        });
                        
                        // 样式还原
                        cssCode.forEach((item, index) => {
                            let reg = new RegExp(`{__CSS_PRECODE${ index + 1 }__}`, 'g');
                            newCode = newCode.replace(reg, item);
                        });
                    }
                    
                    // CSS语法处理
                    if(theLang === 'css') {
                    
                    }
                    
                    // JS语法处理
                    if(theLang === 'javascript') {
                        newCode = preJsCode(newCode);
                    }
                    
                    // 替换内容
                    theDom.innerHTML = `<code class="mc-precode-box">${ newCode }</code>`;
                })();
            }
            
            
            // 遍历处理每一个<pre>标签
            // let preNum = this.aPreDom.length;
            // for(let i = 0; i < preNum; i++) {
            //     // 本条实例内的全部函数
            //     let codeData = [];
            //
            //     let theDom = this.aPreDom[i];
            //
            //     // 加入主题标识
            //     theDom.setAttribute('theme', this.theme);
            //
            //     // 创建一份新代码
            //     let newCode = theDom.innerHTML;
            //
            //     // 保留一份原代码
            //     let oldCode = newCode;
            //
            //     // 逻辑符号
            //     newCode = newCode.replace(/(&&|===|==|\|\||\+=|-=|\*=|\/=|<=|>=|&amp;&amp;)/g, (s1) => {
            //         // console.log(s1);
            //         codeData.push(`<code>${ s1 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 箭头函数
            //     newCode = newCode.replace(/(\s+=>\s+|\s+)(=&gt;)(\s+)/g, (s0, s1, s2, s3) => {
            //         codeData.push(`<code>${ s2 }</code>`);
            //         return `${ s1 }{__PRECODE${ codeData.length }__}${ s3 }`;
            //     });
            //
            //     // HTML标签
            //     newCode = newCode.replace(/(&lt;\/?)(link|a|abbr|acronym|address|applet|area|article|aside|audio|b|base|basefont|bdi|bdo|big|blockquote|body|br|button|canvas|caption|center|cite|code|col|colgroup|command|datalist|dd|del|details|dfn|dialog|dir|div|dl|dt|em|embed|fieldset|figcaption|figure|font|footer|form|frame|frameset|h1|h2|h3|h4|h5|h6|head|header|hr|html|i|iframe|img|input|ins|kbd|keygen|label|legend|li|link|main|map|mark|menu|menuitem|meta|meter|nav|noframes|noscript|object|ol|optgroup|option|output|p|param|progress|q|rp|rt|ruby|s|samp|script|section|select|small|source|span|strike|strong|style|sub|summary|sup|table|tbody|td|textarea|tfoot|th|thead|time|title|tr|track|tt|u|ul|var|video|wbr)(\s*.*?)(&gt;)/gm, (s0, s1, s2, s3, s4) => {
            //
            //         // console.log('120', s3);
            //
            //         s3 = s3.replace(/(\w+)(=)('|")(.*?)(\3)/g, (s0, s1, s2, s3, s4, s5) => {
            //             return `<code class="co-oth">${ s1 }</code><code class="co-equ">${ s2 }</code><code class="co-str">${ s3 }${ s4 }${ s5 }</code>`;
            //         });
            //
            //         codeData.push(`<code class="co-tag"><code>${ s1 }</code><code>${ s2 }</code><code>${ s3 }</code><code>${ s4 }</code></code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 尖括号
            //     // console.log('标签检测');
            //     newCode = newCode.replace(/(<|>|&lt;|&gt;)/g, (s1) => {
            //         // console.log(s1);
            //         if(s1 === '<' || s1 === '&lt;') {
            //             codeData.push(`&lt;`);
            //         }
            //
            //         if(s1 === '>' || s1 === '&gt;') {
            //             codeData.push(`&gt;`);
            //         }
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 正则
            //     newCode = newCode.replace(/(\.replace\()(\/)([^\n]*)(\/)(g?|i?|m?|s?|u?|y?)(,)/g, ($0, $1, $2, $3, $4, $5, s6) => {
            //         // 正则内部处理
            //         $3 = regCode($3);
            //         codeData.push(`<code class="co-reg">${ $2 }</code><code class="co-reg-str">${ $3 }</code><code class="co-reg">${ $4 }</code><code class="co-str">${ $5 }</code><code class="co-sym">${ s6 }</code>`);
            //         return `${ $1 }{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 正则-字符串
            //     newCode = newCode.replace(/(\/)([^\n]*)(\/)(\.test\()/g, ($0, $1, $2, $3, $4) => {
            //         // 正则内部处理
            //         $2 = regCode($2);
            //         codeData.push(`<code class="co-reg">${ $1 }<code class="co-reg-str">${ $2 }</code>${ $3 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}${ $4 }`;
            //     });
            //
            //     // 正则
            //     newCode = newCode.replace(/(new\sRegExp\()('|")([^\n]*)(\2)\s?(,)\s?('|")(g?|i?|m?|s?|u?|y?)(\6)/g, ($0, $1, $2, s3, $4, $5, $6, $7, $8) => {
            //         // 正则内部处理
            //         s3 = regCode(s3);
            //         codeData.push(`<code class="co-str">${ $2 }</code><code class="co-reg"><code class="co-reg-str">${ s3 }</code><code class="co-str">${ $4 }</code><code class="co-sym">${ $5 }</code><code class="co-str">${ $6 }${ $7 }${ $8 }</code></code>`);
            //         return `${ $1 }{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 字符串
            //     newCode = newCode.replace(/('|")[^\n(\1)]*?\1/g, ($1) => {
            //         codeData.push(`<code class="co-str">${ $1 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 单行注释
            //     // console.log('单行注释');
            //     newCode = newCode.replace(/(\/\/[^:][^\n]*)/g, ($1) => {
            //         codeData.push(`<code class="co-row-comments">${ $1 }<code class="co-row-comments-end"></code></code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 多行注释(单星号)
            //     // console.log('多行注释(单星号)');
            //     newCode = newCode.replace(/(\/\*[^*][\s\S]*?\*\/)/g, (g1) => {
            //         codeData.push(`<code class="co-more-comments">${ g1 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 多行注释(双星号)
            //     // console.log('多行注释(双星号)');
            //     newCode = newCode.replace(/(\/\*\*[\s\S]*?\*\/)/g, ($1) => {
            //         // @param
            //         $1 = $1.replace(/(@param)(\s+)/g, '<code class="co-com-param">$1</code>$2');
            //         codeData.push(`<code class="co-mode-comments">${ $1 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 函数变量
            //     newCode = newCode.replace(/\b(let\s+|var\s+)(\w+)(\s*=\s*)/gm, ($0, $1, $2, $3) => {
            //         codeData.push(`<code class="co-var">${ $2 }</code>`);
            //         return `${ $1 }{__PRECODE${ codeData.length }__}${ $3 }`;
            //     });
            //
            //     // 事件名称
            //     newCode = newCode.replace(/(\.)(\w+)(\s*=\s*)(\(|function\s*\()/g, (s0, s1, s2, s3, s4) => {
            //         codeData.push(`<code class="co-event">${ s2 }</code>`);
            //         return `${ s1 }{__PRECODE${ codeData.length }__}${ s3 }${ s4 }`;
            //     });
            //
            //     // 函数名称
            //     newCode = newCode.replace(/(\.|\b)(\w*)(\()/g, (s0, $1, $2, $3) => {
            //         codeData.push(`<code class="co-fun">${ $2 }</code>`);
            //         return `${ $1 }{__PRECODE${ codeData.length }__}${ $3 }`;
            //     });
            //
            //     // 关键词
            //     newCode = newCode.replace(/(\s*|\b)(let|var|import|from|class|new|this|export\sdefault)(\s+|\b)/g, (s0, s1, s2, s3) => {
            //         codeData.push(`<code class="co-ret">${ s2 }</code>`);
            //         return `${ s1 }{__PRECODE${ codeData.length }__}${ s3 }`;
            //     });
            //
            //     // 逻辑符
            //     // newCode = newCode.replace(/(\s*|\b)(false|true|for|if|else|return)(\s+|\b)/g, '$1<code class="co-logic">$2</code>$3');
            //     newCode = newCode.replace(/(\s*|\b)(false|true|for|if|else|return)(\s+|\b)/g, (s0, s1, s2, s3) => {
            //         codeData.push(`<code class="co-logic">${ s2 }</code>`);
            //         return `${ s1 }{__PRECODE${ codeData.length }__}${ s3 }`;
            //     });
            //
            //     // 对象属性
            //     // newCode = newCode.replace(/([^=][,{\s\n]+)([^,\n\s.({})':=]*)(\s*:)/g, '$1<code class="co-key">$2</code>$3');
            //     newCode = newCode.replace(/([^=][,{\s\n]+)([^,\n\s.({})':=]*)(\s*:)/g, (s0, s1, s2, s3) => {
            //         codeData.push(`<code class="co-key">${ s2 }</code>`);
            //         return `${ s1 }{__PRECODE${ codeData.length }__}${ s3 }`;
            //     });
            //
            //     // 对象子集
            //     newCode = newCode.replace(/(\.)([a-zA-Z]+[0-9]*)\b/g, (s0, s1, s2) => {
            //         codeData.push(`<code class="co-sub">${ s2 }</code>`);
            //         return `${ s1 }{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 数字
            //     // console.log('数字处理');
            //     newCode = newCode.replace(/([\s+\+=\-\*\/:,])(-?\d+\.?\d*)\b/g, (s0, s1, s2) => {
            //         codeData.push(`<code class="co-num">${ s2 }</code>`);
            //         return `${ s1 }{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 符号
            //     newCode = newCode.replace(/(,|;)/gm, (s1) => {
            //         codeData.push(`<code class="co-sym">${ s1 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 运算符处理
            //     newCode = newCode.replace(/(\+|\*)/g, (s1) => {
            //         codeData.push(`<code class="co-ope">${ s1 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 点，冒号，括号
            //     newCode = newCode.replace(/(\.|:|\(|\)|\[|\]|\?)/g, (s1) => {
            //         codeData.push(`<code class="co-dot">${ s1 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 等于号
            //     newCode = newCode.replace(/(=)/g, (s1) => {
            //         codeData.push(`<code class="co-equ">${ s1 }</code>`);
            //         return `{__PRECODE${ codeData.length }__}`;
            //     });
            //
            //     // 其它单词
            //     newCode = newCode.replace(/(}\s*)([\w$]+)(\s*{)/g, (s0, s1, s2, s3) => {
            //         codeData.push(`<code class="co-oth">${ s2 }</code>`);
            //         return `${ s1 }{__PRECODE${ codeData.length }__}${ s3 }`;
            //     });
            //
            //     // 内容还原
            //     codeData.forEach((item, index) => {
            //         let reg = new RegExp(`{__PRECODE${ index + 1 }__}`, 'g');
            //         newCode = newCode.replace(reg, item);
            //     });
            //     codeData.forEach((item, index) => {
            //         let reg = new RegExp(`{__PRECODE${ index + 1 }__}`, 'g');
            //         newCode = newCode.replace(reg, item);
            //     });
            //     codeData.forEach((item, index) => {
            //         let reg = new RegExp(`{__PRECODE${ index + 1 }__}`, 'g');
            //         newCode = newCode.replace(reg, item);
            //     });
            //
            //     // 注释再次处理
            //     newCode = newCode.replace(/<code class="co-row-comments".*<code class="co-row-comments-end"><\/code><\/code>/gm, (s1) => {
            //         // console.log(s1);
            //         s1 = '<code class="co-row-comments">' + s1.replace(/<code[^>]*>/g, '').replace(/<\/code>/g, '') + '</code>';
            //         return s1;
            //     });
            //
            //     // 插入页面
            //     theDom.innerHTML = newCode;
            // }
            
        });
        
    }
}

window.fekit ? window.fekit.McPreCode = McPrecode : window.fekit = {McPreCode: McPrecode};
export default McPrecode;


